FROM golang:latest AS builder

WORKDIR /src
COPY . .
RUN go build -v -mod=vendor

FROM hub.dianchu.cc/library/gobase:latest
WORKDIR /bin
COPY --from=builder /src/awesomeProject .

CMD ["./awesomeProject"]
